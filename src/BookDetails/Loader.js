import {Component} from "react";

class Loader extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <div className="loader">
                <div className="loading-line"></div>
                <div className="loading-line"></div>
                <div className="loading-line"></div>
                <div className="loading-line"></div>
                <div className="line2"></div>
            </div>
        );
    }
}

export default Loader;