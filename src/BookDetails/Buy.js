import {Component} from "react";
import {Link} from "react-router-dom";

class Buy extends Component {
    constructor(props) {
        super(props);
        this.state = {buyQuantity:1}
    }

    setQuantity = (e) => {
        this.setState(
            {
                buyQuantity: e.target.value,
            }
        );
    }

    render() {
        return(
            <div className="quantity">
                {(this.props.count > 0)
                &&
                <div>
                    <span>Quantity: </span>
                    <input type="number" min="0" max={this.props.count}
                           onChange={this.setQuantity}
                           placeholder="Enter Qty"
                           style={{width: "85px", height: "50px", textAlign: "center"}}
                           defaultValue="1"
                    />
                    <Link to={{
                        pathname: "/order/new",
                        state: {
                            bookId: this.props.params,
                            bookCount: this.state.buyQuantity,
                            imageUrl: this.props.data.imageUrl,
                            bookName: this.props.data.name,
                            bookPrice: this.props.data.price
                        }
                    }}
                          className="btn buyBtn rounded-pill d-flex justify-content-center align-items-center"
                          style={{backgroundColor: '#ffa41b', marginTop: "-5px", marginLeft: "10px"}}
                    >
                        <span className="buyText">Buy</span>
                    </Link>
                </div>
                }
            </div>
        );
    }
}

export default Buy;