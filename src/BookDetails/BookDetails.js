import {Component} from "react";
import './BookDetails.css';
import {Col, Container, Row} from "react-bootstrap";
import Fade from "react-reveal/Fade";
import {withRouter} from "react-router-dom";
import runtimeEnv from '@mars/heroku-js-runtime-env';
import "../HomePage/Loader.css";
import Details from "./Details";
import Buy from "./Buy";
import Loader from "./Loader";


class BookDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bookDetails: null,
            loading: true,
            emptyDetails: false,
            buyQuantity: 1,
            stock: false
        }
    }

    setDetails = (data) => {
        if (data !== null) {
            if (typeof data.imageUrl !== 'undefined' && data.imageUrl !== null) {
                if (data.imageUrl.toString().slice(8, 14) === "images") {
                    const index = data.imageUrl.lastIndexOf("/") - 1;
                    const length = data.imageUrl.length;
                    data.imageUrl = data.imageUrl.substr(0, index) + "l"
                        + data.imageUrl.substr(index + 1, length);
                }
            }

            return this.setState(
                {
                    bookDetails: data,
                    loading: false,
                    stock: data.booksCount > 0 ? 1 : 0
                }
            )
        } else {
            return this.setState(
                {
                    emptyDetails: true,
                }
            )
        }
    }

    async componentDidMount() {
        const env = runtimeEnv();
        let URL = env.REACT_APP_API_URL + 'books/';
        let bookId = this.props.match.params.bookId;
        const response = await fetch(URL + bookId);
        const parsedJson = await response.json();
        this.setDetails(parsedJson);
    }

    render() {

        if (this.state.emptyDetails) {
            return <div className="text-center" style={{fontSize: "2rem", color: "#ee2c6b"}}>Book not found</div>
        }
        if (this.state.loading) {
            return <Loader />
        }
        const {imageUrl, booksCount} = this.state.bookDetails;
        return (
            <Container className="parent-div">
                <div id="details">
                    <Row className="my-5">
                        <Col md={4} className="text-center">
                            <img className="image" src={imageUrl} alt="book"/>
                        </Col>
                        <Col md={8}>
                            <Fade bottom>
                                <Details details={this.state.bookDetails} />
                            </Fade>
                        </Col>
                    </Row>
                    <Row className="my-5 btn-row">
                        <Col md={4} className="my-auto col-left">
                            <div>
                                <Row className="qty-row">
                                    <Buy params={this.props.match.params.bookId} count={this.state.bookDetails.booksCount}
                                         data={this.state.bookDetails} buyQty={this.state.buyQuantity}
                                    />
                                    {!(booksCount) && <span className="no-stock">Out of stock!</span>}
                                </Row>
                                <Row className="available-row">
                                    {(booksCount > 0) &&
                                    <div style={{marginLeft:"145px"}}>
                                        <div>{booksCount} available</div>
                                    </div>
                                    }
                                </Row>
                            </div>
                        </Col>
                        <Col md={8} className="d-flex justify-content-left col-right">
                        </Col>
                    </Row>
                </div>
            </Container>
        );
    }
}

export default withRouter(BookDetails);