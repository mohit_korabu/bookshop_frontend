import {Component} from "react";
import {Rating} from "@material-ui/lab";
import {Row} from "react-bootstrap";

class Details extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {name, authorName, price, ratings, isbn} = this.props.details;
        return (
            <div>
                <h1 className="name">{name}</h1>
                <div className="author-name"><span>by {authorName}</span></div>
                <div>
                    <Rating
                        size="large"
                        name="half-rating-read"
                        defaultValue={ratings}
                        precision={0.1}
                        readOnly
                    />
                </div>
                <hr className="line"/>
                <div className="details">
                    <Row style={{marginLeft: "25px"}}>
                        <Row md={4} className="priceVal">
                            &#x20b9;{price}
                        </Row>
                        <Row md={4} className="right-details-col">
                            <Row className="row-pad">
                                {(isbn > 0) && <span>ISBN: {isbn}</span>}
                                {!(isbn) && <span>ISBN not available</span>}
                            </Row>
                        </Row>
                    </Row>
                </div>
            </div>
        );
    }


}

export default Details;