import {Component} from "react";
import '../BaseTemplate.css';
import SearchBar from "./SearchBar/SearchBar";
import {BrowserRouter} from "react-router-dom";


class SearchAndAccountBar extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="row marg">
                <div className="col-md-3 col-md-push-1">
                    <div className="shrvenk"></div>
                </div>
                <BrowserRouter>
                    <SearchBar/>
                </BrowserRouter>
            </div>
        );
    }
}

export default SearchAndAccountBar;