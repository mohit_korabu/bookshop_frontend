import {Component} from "react";
import {withRouter} from "react-router";
import "../BaseTemplate.css"

class Back extends Component {
    render() {
        return (
            <a id="backLink" style={{cursor: "pointer", paddingTop:"0px", paddingBottom:"0px"}}
               onClick={() => this.props.history.go(-1)}>
                <button className="myButton">
                </button>
            </a>
        );
    }
}

export default withRouter(Back);