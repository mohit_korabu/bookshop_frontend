import {fireEvent, render} from '@testing-library/react';
import {createMemoryHistory} from "history";
import {Route, Router} from "react-router-dom";
import Back from "./Back";

test('should render Back button', () => {
    const history = createMemoryHistory();
    history.push('/');
    const result = render(
        <Router history={history}>
            <Route path="/">
                <Back />
            </Route>
        </Router>
    );
    const button = result.container.querySelector('#backLink');
    fireEvent.click(button);
});