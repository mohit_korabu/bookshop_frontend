import {fireEvent, render, screen} from '@testing-library/react';
import SearchBar from "./SearchBar";
import {BrowserRouter} from "react-router-dom";
import {createMemoryHistory} from "history";


test('should show search bar', () => {
    const history = createMemoryHistory()
    render(
        <BrowserRouter history={history}>
            <SearchBar/>
        </BrowserRouter>
    )
    expect(screen.getByRole('textbox')).toBeInTheDocument();
});

test('should show booklist with search', () => {
    const history = createMemoryHistory()
    render(
        <BrowserRouter history={history}>
            <SearchBar/>
        </BrowserRouter>
    )
    fireEvent.change(screen.getByRole('textbox'), {target: {value: "city"}});
    fireEvent.submit(screen.getByRole('textbox'), {target: {value: "city"}});
    expect(screen.getByRole('textbox')).toBeInTheDocument();
});