import {Component} from "react";
import '../../BaseTemplate.css';
import {withRouter} from "react-router-dom";

class SearchBar extends Component {
    constructor(props) {
        super(props);
        const query = new URLSearchParams(this.props.location.search);
        const searchKeyWord = query.get("search") || "";
        this.state = {search: searchKeyWord};
    }

    handleInputChange(evt){
        this.setState({search: evt.target.value});
    }

    handleFormSubmit = () =>{
        const link = '/?search='+ `${this.state.search}`;
        this.props.history.push({link});
    }

    render() {
        return (
            <div className="col-md-4 col-md-push-1">
                <form onSubmit={this.handleFormSubmit}>
                    <div className="active-pink-3 active-pink-4 mb-4">
                        <input onChange={this.handleInputChange.bind(this)} className="form-control srh" name='search'
                               type="text" value={this.state.search} placeholder="Search by Book Name or Details" aria-label="Search"/>
                    </div>
                </form>
            </div>
        );
    }
}

export default withRouter(SearchBar);