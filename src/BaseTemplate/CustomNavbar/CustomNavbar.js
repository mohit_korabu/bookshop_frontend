import {Component} from "react";
import '../BaseTemplate.css';
import Back from "../SearchAndAccountBar/Back";

class CustomNavbar extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="row navmag">
                <nav className="navbar navbar-inverse">
                    <div className="container-fluid">
                        <ul className="nav navbar-nav left-nav">
                            <li>
                                <a href="/" style={{paddingTop: "0px", paddingBottom: "0px"}}>
                                    <span style={{color: "white"}}>
                                        <button className="homeButton"/>
                                    </span>
                                </a>
                            </li>
                        </ul>
                        <ul className="nav navbar-nav right-nav">
                            <li><Back/></li>
                        </ul>
                    </div>
                </nav>
            </div>
        );
    }
}

export default CustomNavbar;