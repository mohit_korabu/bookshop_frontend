import {render} from '@testing-library/react';
import BaseTemplate from "./BaseTemplate";
import {createMemoryHistory} from "history";
import {Route, Router} from "react-router-dom";


test('should show Home and Contact', () => {
    const history = createMemoryHistory();
    history.push('/');
    render(
        <Router history={history}>
            <Route path="/">
                <BaseTemplate />
            </Route>
        </Router>
    );
});