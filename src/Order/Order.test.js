import {fireEvent, render, screen} from '@testing-library/react';
import Order from "./Order";
import {Route, Router} from "react-router-dom";
import {createMemoryHistory} from "history";
import runtimeEnv from "@mars/heroku-js-runtime-env";

beforeAll(() => jest.spyOn(window, 'fetch'))

test("does have a form ", () => {
    const history = createMemoryHistory()
    history.push('/order/new', {bookId: 4, bookCount: 5})
    render(
        <Router history={history}>
            <Route exact path="/order/new">
                <Order/>
            </Route>
        </Router>
    )
    expect(screen.getByRole('form')).toBeInTheDocument();
})

test("has inputs to receive new delivery details", () => {
    const history = createMemoryHistory()
    history.push('/order/new', {bookId: 4, bookCount: 5})
    render(
        <Router history={history}>
            <Route exact path="/order/new">
                <Order/>
            </Route>
        </Router>
    )

    expect(screen.getByPlaceholderText("Enter Your Name")).toBeInTheDocument();
    expect(screen.getByPlaceholderText("Enter Your Email")).toBeInTheDocument();
    expect(screen.getByPlaceholderText("Enter Your Address")).toBeInTheDocument();
    expect(screen.getByLabelText("Email : *")).toBeInTheDocument();
    expect(screen.getByRole('button', {name: "Place Order"})).toBeInTheDocument();

})

test("should be able to change form values", () => {
    const history = createMemoryHistory()
    history.push('/order/new', {bookId: 4, bookCount: 5})
    render(
        <Router history={history}>
            <Route exact path="/order/new">
                <Order/>
            </Route>
        </Router>
    )
    let name = screen.getByLabelText("Name : *");

    fireEvent.change(name, {target: {value: "test Name"}})
    expect(name.value).toBe("test Name");

})

test("should be able to fetch when submitted", async () => {
    const history = createMemoryHistory()
    history.push('/order/new', {bookId: '4', bookCount: '5'})
    render(
        <Router history={history}>
            <Route exact path="/order/new">
                <Order/>
            </Route>
        </Router>
    )

    const response = {
        address: "Hostel 4",
        bookCount: 5,
        bookName: "Stephenie Meyer",
        country: "India",
        email: "193050051@iitb.ac.in",
        mobileNumber: "09193050051",
        name: "Saurabh Warade",
        orderId: 136,
        totalAmount: 11675
    };

    window.fetch.mockResolvedValueOnce({
        ok: true,
        json: async () => response,
    });


    let name = screen.getByLabelText("Name : *");
    fireEvent.change(name, {target: {value: "Saurabh Warade"}})
    let email = screen.getByLabelText("Email : *");
    fireEvent.change(email, {target: {value: "193050051@iitb.ac.in"}})
    let mobileNumber = screen.getByLabelText("Mobile : *");
    fireEvent.change(mobileNumber, {target: {value: "09193050051"}})
    let address = screen.getByLabelText("Address : *");
    fireEvent.change(address, {target: {value: "Hostel 4"}})
    let country = screen.getByLabelText("Country : *");
    fireEvent.change(country, {target: {value: "India"}})
    let bookCount = screen.getByLabelText("BookCount :");
    fireEvent.change(bookCount, {target: {value: 5}})

    const data = {
        name: "Saurabh Warade",
        email: "193050051@iitb.ac.in",
        mobileNumber: "09193050051",
        address: "Hostel 4",
        country: "India",
        bookId: "4",
        bookCount: "5"
    }
    const env = runtimeEnv();

    let submitButton = screen.getByRole("button", {name: "Place Order"});
    fireEvent.submit(submitButton);

    expect(window.fetch).toHaveBeenCalledWith(env.REACT_APP_API_URL + "books/order", {
        body: JSON.stringify(data),
        headers: {"Content-Type": "application/json"},
        method: "POST"
    },);

    expect(window.fetch).toHaveBeenCalledTimes(1);
})
