import {createMemoryHistory} from "history";
import {fireEvent, render, screen} from "@testing-library/react";
import {Route, Router} from "react-router-dom";
import OrderStatus from "./OrderStatus";

test("should display confirm order", () => {
    const history = createMemoryHistory()
    history.push('/order/150/status',
        {statusMessage: {message: "Order Confirmed"}})

    render(
        <Router history={history}>
            <Route path="/order/:orderId/status">
                <OrderStatus/>
            </Route>
        </Router>
    );

    expect(screen.getByText("Order Placed Successfully")).toBeInTheDocument();
})

test("should display confirm order", () => {
    const history = createMemoryHistory()
    history.push('/order/150/status',
        {statusMessage: {message: "Late confirmation ,Book out of stock"}})

    render(
        <Router history={history}>
            <Route path="/order/:orderId/status">
                <OrderStatus/>
            </Route>
        </Router>
    );

    expect(screen.getByText("Late confirmation ,Book out of stock")).toBeInTheDocument();
})

test("should be able to redirect to home", () => {
    const history = createMemoryHistory()
    history.push('/order/150/status',
        {statusMessage: {message: "Order Already Confirmed"}})

    render(
        <Router history={history}>
            <Route path="/order/:orderId/status">
                <OrderStatus/>
            </Route>
        </Router>
    );

    const clickButton = screen.getByRole("button");
    expect(screen.getByText("Order Already Confirmed")).toBeInTheDocument();

    fireEvent.click(clickButton);
})