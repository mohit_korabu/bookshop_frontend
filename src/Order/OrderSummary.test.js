import {createMemoryHistory} from "history";
import {Route, Router} from "react-router-dom";
import OrderSummary from "./OrderSummary";
import {fireEvent, render, screen, waitFor} from '@testing-library/react';
import runtimeEnv from "@mars/heroku-js-runtime-env";
import OrderStatus from "./OrderStatus";

beforeAll(() => jest.spyOn(window, 'fetch'))

test("should be able to fetch when order confirmed", async () => {
    const summary = {
        address: "Hostel 4",
        bookCount: 1,
        bookName: "The Hitchhiker's Guide to the Galaxy (Hitchhiker's Guide to the Galaxy, #1)",
        country: "India",
        email: "193050051@iitb.ac.in",
        mobileNumber: "09193050051",
        name: "Saurabh Warade",
        orderId: 180,
        totalAmount: 3074
    }

    const history = createMemoryHistory()
    history.push('/order/180')
    const env = runtimeEnv();

    window.fetch.mockResolvedValue(env.REACT_APP_API_URL + "orders/180", {
        ok: true,
        json: async () => summary,
    });
    const response = {message: "Order Confirmed"}

    window.fetch.mockResolvedValue({
        ok: true,
        json: async () => response,
    });

    render(
        <Router history={history}>
            <Route exact path="/order/:orderId">
                <OrderSummary/>
            </Route>
        </Router>
    )

    await waitFor(() => screen.getByText("Order Details"));

    const clickButton = screen.getByRole("button");
    fireEvent.click(clickButton);
    expect(window.fetch).toHaveBeenCalledWith(env.REACT_APP_API_URL + "books/order/180/confirm", {method: "PUT"});
    expect(window.fetch).toHaveBeenCalledWith(env.REACT_APP_API_URL + "orders/180", {method: "GET"});
    expect(window.fetch).toHaveBeenCalledTimes(2);
})
