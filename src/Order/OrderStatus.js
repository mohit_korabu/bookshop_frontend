import {Component} from "react";
import {Button, Container} from "react-bootstrap";
import "./OrderStatus.css"
import {withRouter} from "react-router-dom";
import Tada from 'react-reveal/Tada';
import Fade from "react-reveal/Fade";

class OrderStatus extends Component {
    constructor(props) {
        super(props);
    }

    handleOnclick() {
        this.props.history.push("/")
    }

    render() {
        if(this.props.location.state === undefined) return <div style={{textAlign:"center"}}>No summary present</div>
        if (this.props.location.state.statusMessage.message === "Order Confirmed") {
            return (
                <Fade>
                    <Container className="ordered-div">
                        <h2 className="OrderPlaced">
                            Order Placed Successfully
                            <Tada duration={3000}>
                                <div style={{marginTop: "10px"}}>
                                    <div className="check"></div>
                                </div>
                            </Tada>
                        </h2>
                        <div className="buyMoreButton-div">
                            <Button className="buyMoreButton" onClick={this.handleOnclick.bind(this)}>Buy More</Button>
                        </div>
                    </Container>
                </Fade>

            )
        }
        return (
            <Container className="ordered-div">
                <h2 className="OrderNotPlaced">{this.props.location.state.statusMessage.message} <span>&#10060;</span>
                </h2>
                <div className="buyMoreButton-div">
                    <Button className="buyMoreButton" onClick={this.handleOnclick.bind(this)}>Explore</Button>
                </div>
            </Container>
        )


    }
}

export default withRouter(OrderStatus);