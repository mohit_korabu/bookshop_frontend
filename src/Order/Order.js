import {Component} from "react";
import {Redirect, withRouter} from "react-router-dom";
import "./Order.css"
import {Container} from "react-bootstrap";
import runtimeEnv from '@mars/heroku-js-runtime-env';
import Fade from "react-reveal/Fade";

class Order extends Component {

    constructor(props) {
        super(props);
        this.state = {
            bookId: null, bookCount: null, name: null,
            email: null, mobileNumber: null, address: null, country: null,
            summary: {},
            isSubmitted: false,
            isOrderValid: true,
            imageUrl: null,
            bookName: null,
            bookPrice: null
        }
    }

    componentDidMount() {
        const bookId = this.props.location.state.bookId;
        const bookCount = this.props.location.state.bookCount;
        const imageUrl = this.props.location.state.imageUrl;
        const bookName = this.props.location.state.bookName;
        const bookPrice = this.props.location.state.bookPrice;
        this.setState({
            bookId: bookId,
            bookCount: bookCount,
            bookName: bookName,
            imageUrl: imageUrl,
            bookPrice: bookPrice
        });
    }

    handleInputChange(evt) {
        this.setState({[evt.target.name]: evt.target.value})
    }


    async postData(url, data) {

        const response = await fetch(url, {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(data)
        });
        return response.json()
            .then(json => this.setState({summary: json}))
    }

    async handleFormSubmit(evt) {

        evt.preventDefault()
        const env = runtimeEnv();
        const url = env.REACT_APP_API_URL + 'books/order';
        const {bookId, address, country, name, email, bookCount, mobileNumber} = this.state;
        const data = {name, email, mobileNumber, address, country, bookId, bookCount};
        await this.postData(url, data)
        if (this.state.summary.message != null) {
            this.setState({isOrderValid: false})
        } else {
            this.setState({isOrderValid: true})
            this.setState({isSubmitted: true})
        }

    }

    checkInvalidOrderMessage() {
        if (!this.state.isOrderValid)
            return (this.state.summary.message);

    }

    render() {
        if (this.state.isSubmitted && this.state.isOrderValid) {
            return (
                <Redirect push to={{
                    pathname: `/order/${this.state.summary.orderId}`
                }}
                />
            )
        }

        return (
            <Container className="order-div">
                <Fade>
                    <div>
                        <div className="bookNaming"><h3 className={"card-title"}>{this.state.bookName}</h3></div>
                        <div className="content">
                            <div className="bookDetailing">
                                <img className="image" src={this.state.imageUrl} alt="book"/>
                                <h2 className="imagePrice">₹{this.state.bookPrice}</h2>
                            </div>

                            <div className="form-div">
                                <form className="form" name="order-form" onSubmit={this.handleFormSubmit.bind(this)}>
                                    <div className="shipping">
                                        <h3>Add Shipping Details</h3>
                                    </div>
                                    <label className="userName">
                                        Name : <span className="required">*</span>
                                        <input
                                            value={this.state.name}
                                            name="name"
                                            type="text"
                                            placeholder="Enter Your Name"
                                            onChange={this.handleInputChange.bind(this)}
                                            required
                                        />
                                    </label>
                                    <br/>

                                    <label className="email">
                                        Email : <span className="required">*</span>
                                        <input
                                            value={this.state.email}
                                            name="email"
                                            type="email"
                                            placeholder="Enter Your Email"
                                            onChange={this.handleInputChange.bind(this)}
                                            required
                                        />
                                    </label>
                                    <br/>
                                    <label className="mobile">
                                        Mobile : <span className="required">*</span>
                                        <input type="tel"
                                               pattern="[6-9]{1}[0-9]{9}"
                                               value={this.state.mobileNumber}
                                               name="mobileNumber"
                                               placeholder="Enter Your Mobile number"
                                               onChange={this.handleInputChange.bind(this)}
                                               required
                                        />
                                    </label>
                                    <br/>
                                    <label className="address">
                                        Address : <span className="required">*</span>
                                        <input
                                            value={this.state.address}
                                            name="address"
                                            type="text"
                                            placeholder="Enter Your Address"
                                            onChange={this.handleInputChange.bind(this)}
                                            required
                                        />
                                    </label>
                                    <br/>
                                    <label className="country">
                                        Country : <span className="required">*</span>
                                        <input
                                            value={this.state.country}
                                            name="country"
                                            type="text"
                                            placeholder="Enter Your Country"
                                            onChange={this.handleInputChange.bind(this)}
                                            required
                                        />
                                    </label>
                                    <br/>
                                    <label className="bookCount">
                                        BookCount :
                                        <input
                                            value={this.state.bookCount}
                                            name="bookCount"
                                            type="number"
                                            min="1"
                                            placeholder="Enter Book Count"
                                            onChange={this.handleInputChange.bind(this)}
                                            required
                                        />
                                    </label>
                                    <br/>
                                    <input type="submit" value="Place Order"/>
                                    <p>
                                        <span className="required">*</span>Required field
                                    </p>
                                </form>

                            </div>
                        </div>
                        <p className="bookOut">{this.checkInvalidOrderMessage()}</p>
                    </div>
                </Fade>
            </Container>

        )
    };

}

export default withRouter(Order);