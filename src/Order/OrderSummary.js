import {Component} from "react";
import {Redirect, withRouter} from "react-router-dom";
import './OrderSummary.css'
import {Button, Container} from "react-bootstrap";
import runtimeEnv from "@mars/heroku-js-runtime-env";
import Fade from "react-reveal/Fade";
import Loader from "../BookDetails/Loader";


class OrderSummary extends Component {
    constructor(props) {
        super(props);
        this.state = {summary: {}, imageUrl: null, status: false, orderPlacedMessage: {}, loading: true, statusMessage: null}
    }

    async putData(url) {

        const response = await fetch(url, {
            method: 'PUT'
        });
        return response.json()
            .then(json => this.setState({orderPlacedMessage: json}));
    }

    async handleClick() {
        const env = runtimeEnv();
        const url = `${env.REACT_APP_API_URL}books/order/${this.props.match.params.orderId}/confirm`
        await this.putData(url);
        this.setState({status: true})

    }

    handleOnclick() {
        this.props.history.push("/")
    }

    render() {

        if (this.state.status) {
            return (
                <Redirect push to={{
                    pathname: `/order/${this.props.match.params.orderId}/status`,
                    state: {statusMessage: this.state.orderPlacedMessage}
                }}
                />
            )
        }
        if (this.state.loading) {
            return (<Loader/>);
        }

        if (this.state.summary.message === "Order Not Found") {
            return (
                <Container>
                    <div className="OrderInvalid"><h2 className="InvalidOrder">No Such Order Exist</h2>
                        <div className="buyMoreButton-div">
                            <Button className="buyMoreButton" onClick={this.handleOnclick.bind(this)}>Buy More</Button>
                        </div>
                    </div>
                </Container>
            )

        }

        return (
            <Container className="summary-div">
                <Fade>
                    <div>
                        <div className='OrderSummary'>
                            <h1>Order Summary</h1>
                            <div className='centerBox'>
                                <div>
                                    <img className="bookImage" src={this.state.summary.imageUrl} alt="book"/>
                                </div>
                                <div className="OrderDetails">
                                    <h2>Order Details</h2>
                                    <p>Order ID : {this.props.match.params.orderId}</p>
                                    <p className="bookName">Book Name : {this.state.summary.bookName}</p>
                                    <p>Quantity : {this.state.summary.bookCount}</p>
                                    <p>Total Amount : ₹{this.state.summary.totalAmount}</p>
                                </div>
                                <div className="ShippingDetails">
                                    <h2>Shipping Details</h2>
                                    <p className="pTag">Name : {this.state.summary.name}</p>
                                    <p className="pTag">Contact : {this.state.summary.mobileNumber}</p>
                                    <p className="pTag">Address : {this.state.summary.address}</p>
                                    <p className="pTag">Country : {this.state.summary.country}</p>
                                </div>
                            </div>
                        </div>
                        {!(this.state.summary.confirmation) &&
                        <Button className='confirmOrder' onClick={this.handleClick.bind(this)}>Confirm Order</Button>}
                        {(this.state.summary.confirmation) && <h2 className="confirmation">Order Already Confirmed</h2>}
                    </div>

                </Fade>
            </Container>
        )
    }

    async componentDidMount() {
        const env = runtimeEnv();
        const url = `${env.REACT_APP_API_URL}orders/${this.props.match.params.orderId}`

        const response = await fetch(url, {
            method: 'GET'
        });
        return response.json()
            .then(json => this.setState({
                summary: json,
                loading: false
            }));
    }


}

export default withRouter(OrderSummary)