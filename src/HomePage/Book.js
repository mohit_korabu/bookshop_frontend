import './Book.css'
import {Link} from "react-router-dom";

export default function Book(props) {

    return <Link to={`/books/${props.book.id}`}>
        <div key={props.book.name} className={"col-md-4 main-container"}>
            <div className={"card-img-top"}><img
                src={props.book.imageUrl === " " ? 'https://dev-bootcamp-book-shop.s3.us-east-2.amazonaws.com/default_book_cover.jpg' : props.book.imageUrl}
                alt={"Nan"}/></div>
            <div className={"card-body"}>
                <h5 className={"card-title"}>{props.book.name}</h5>
                <div className={"authorName"}>By {props.book.authorName}</div>
                <div className={"price"}>₹{props.book.price}</div>
            </div>

        </div>
    </Link>


}