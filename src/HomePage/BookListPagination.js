// import {useEffect, useState} from "react";
// import {Link} from "react-router-dom";
// import PaginatedBookList from "./PaginatedBookList";
// import './BookList.css'
// import './Loader.css'
// import {useLocation} from "react-router";
// import runtimeEnv from "@mars/heroku-js-runtime-env";
// import Fade from "react-reveal/Fade";
// import Loader from "../BookDetails/Loader";
//
//
// function BookListPagination(props) {
//     function useQuery() {
//         return new URLSearchParams(useLocation().search);
//     }
//
//     let query = useQuery();
//     const initialPageNumber = parseInt(query.get("pageNumber") || 1);
//     const [pageNumber, setPageNumber] = useState(initialPageNumber);
//     const [totalPages, setTotalPages] = useState(1);
//     const initialSortOrder = (query.get("order") || 'desc');
//     const [sortOrder, setSortOrder] = useState(initialSortOrder);
//     const searchKeyword = (query.get("search") || "");
//     const [books, updateBooks] = useState([]);
//     const [loading, updateLoading] = useState(true);
//     const pageSize = props.pageSize || 9;
//     const MAX_SIZE = 5000;
//     const pageHandler = (offset) => {
//         if (pageNumber + offset <= totalPages) {
//             setPageNumber(pageNumber + offset);
//         }
//         updateLoading(true);
//     }
//     const previousPageHandler = () => {
//         if (pageNumber > 1) {
//             setPageNumber(pageNumber - 1);
//         }
//         updateLoading(true);
//     }
//
//     const sortOrderHandler = (offset) => {
//         setPageNumber(1);
//         if (offset !== sortOrder) {
//             setSortOrder(offset);
//             updateLoading(true);
//         }
//     }
//
//     useEffect(() => {
//         const getLength = async () => {
//             const env = runtimeEnv();
//             const URL = env.REACT_APP_API_URL + `books?pageSize=${MAX_SIZE}&pageNumber=1&search=${searchKeyword}&sortBy=price&order=${sortOrder}`;
//             const response = await fetch(URL);
//             const parsedJson = await response.json();
//
//             setTotalPages(Math.ceil(parsedJson.length / pageSize));
//         }
//         getLength();
//     }, []);
//
//     useEffect(() => {
//         const getBooks = async () => {
//             const env = runtimeEnv();
//             const URL = env.REACT_APP_API_URL + `books?pageSize=${pageSize}&pageNumber=${pageNumber}&search=${searchKeyword}&sortBy=price&order=${sortOrder}`;
//             const response = await fetch(URL);
//             const parsedJson = await response.json();
//             updateBooks(parsedJson);
//             updateLoading(false);
//         }
//         getBooks();
//     }, [pageNumber, sortOrder]);
//
//     function constructSearch(pageNumber, sortOrder) {
//         const searchParam = query.get("search");
//         const queryParams = []
//         searchParam && queryParams.push(`search=${searchParam}`);
//         queryParams.push(`pageNumber=${pageNumber}`);
//         queryParams.push(`order=${sortOrder}`);
//         return `?${queryParams.join('&')}`;
//     }
//
//     if (loading) {
//         return (<Loader />);
//     } else if (books.length === 0) {
//         return (
//             <div className="noBooks">
//                 <h3 className="headno">We couldn't find any result for "<span className="skey">{searchKeyword}</span>"
//                 </h3>
//                 <ul>
//                     <li className="inst">check your spelling</li>
//                     <li className="inst">use more generic search terms</li>
//                     <li className="inst">the product you are searching for may be discontinued</li>
//                 </ul>
//                 <a className="linkmarg" href="/">Go to Home</a>
//             </div>
//         )
//     } else
//         return (<div style={{marginLeft: "141px"}}>
//                 <div style={{margin: "135px"}}>
//                     <nav className={"sorting"}
//                          style={{
//                              position: 'absolute',
//                              right: 540,
//                              top: 160,
//                          }}>
//                         <h3>Sort By Price</h3>
//                         <button onClick={() => sortOrderHandler('asc')}
//                                 style={{
//                                     position: 'absolute',
//                                     right: -40,
//                                     top: 22,
//                                 }}>
//                             <Link to={{pathname: '/', search: constructSearch(1, 'asc')}}>
//                                 ▲
//                             </Link>
//                         </button>
//
//                         <button onClick={() => sortOrderHandler('desc')}
//                                 style={{
//                                     position: 'absolute',
//                                     right: -70,
//                                     top: 22,
//                                 }}>
//                             <Link to={{pathname: '/', search: constructSearch(1, 'desc')}}>
//                                 ▼
//                             </Link>
//                         </button>
//                     </nav>
//                 </div>
//
//                 <Fade bottom>
//                     <PaginatedBookList books={books}/>
//                 </Fade>
//
//                 <nav className={"container nav-container"} style={{marginLeft: "141px"}}>
//                     <ul className="pagination">
//                         <li className="page-item" onClick={previousPageHandler}>
//                             {pageNumber !== 1 && <Link
//                                 to={{
//                                     pathname: '/',
//                                     search: constructSearch(((pageNumber - 1) > 0 ? pageNumber - 1 : 1), sortOrder)
//                                 }}>
//                                 Previous
//                             </Link>}
//                         </li>
//                         <li className="page-item" onClick={() => pageHandler(-2)}>
//                             {pageNumber - 2 > 0 &&
//                             <Link to={{pathname: '/', search: constructSearch((pageNumber - 2))}}>
//                                 {isNaN(parseInt(query.get("pageNumber"))) ? 1 : parseInt(query.get("pageNumber")) - 2}
//                             </Link>
//                             }
//                         </li>
//                         <li onClick={() => pageHandler(-1)}>
//                             {pageNumber - 1 > 0 &&
//                             <Link to={{pathname: '/', search: constructSearch((pageNumber - 1))}}>
//                                 {isNaN(parseInt(query.get("pageNumber"))) ? 1 : parseInt(query.get("pageNumber")) - 1}
//                             </Link>
//                             }
//                         </li>
//                         <li className="page-item active">
//                             {books.length !== 0 &&
//                             <Link to={{pathname: '/', search: constructSearch(pageNumber, sortOrder)}}>
//                                 {isNaN(parseInt(query.get("pageNumber"))) ? 1 : parseInt(query.get("pageNumber"))}
//                             </Link>
//                             }
//                         </li>
//                         <li onClick={() => pageHandler(1)}>
//                             {pageNumber + 1 <= totalPages &&
//                             <Link to={{pathname: '/', search: constructSearch((pageNumber + 1), sortOrder)}}>
//                                 {isNaN(parseInt(query.get("pageNumber"))) ? 2 : parseInt(query.get("pageNumber")) + 1}
//                             </Link>
//                             }
//                         </li>
//                         <li className="page-item" onClick={() => pageHandler(2)}>
//                             {pageNumber + 2 <= totalPages &&
//                             <Link to={{pathname: '/', search: constructSearch((pageNumber + 2), sortOrder)}}>
//                                 {isNaN(parseInt(query.get("pageNumber"))) ? 3 : parseInt(query.get("pageNumber")) + 2}
//                             </Link>
//                             }
//                         </li>
//                         <li onClick={() => pageHandler(1)}>
//                             {pageNumber !== totalPages && (books.length !== 0) &&
//                             <Link to={{
//                                 pathname: '/',
//                                 search: constructSearch(((pageNumber + 1) <= totalPages ? pageNumber + 1 : pageNumber), sortOrder)
//                             }}>
//                                 Next
//                             </Link>
//                             }
//                         </li>
//                     </ul>
//                 </nav>
//             </div>
//         );
// }
//
// export default BookListPagination;