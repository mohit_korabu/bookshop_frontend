import {Component} from "react";
import Book from "./Book";

export default class PaginatedBookList extends Component {
    render() {
        return (
            <div className={"container"}>
                {
                    this.props.books.map((book) =>
                        <Book book={book} key={book.id}/>
                    )
                }
            </div>
        )
    }
}