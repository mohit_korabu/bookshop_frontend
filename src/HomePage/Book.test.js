import {render, screen} from "@testing-library/react";
import Book from "./Book";
import {BrowserRouter} from "react-router-dom";

test('Renders Book', () => {
    const book = {
        "name": "City of Bones (The Mortal Instruments, #1)1",
        "price": 14611,
        "authorName": "Cassandra Clare1",
        "imageUrl": "https://images.gr-assets.com/books/1432730315m/256683.jpg"
    };
    render(<BrowserRouter><Book book={book}/></BrowserRouter>);
    expect(screen.getByText('City of Bones (The Mortal Instruments, #1)1')).toBeInTheDocument();
    expect(screen.getByText('₹14611')).toBeInTheDocument();
    expect(screen.getByText('By Cassandra Clare1')).toBeInTheDocument();
});

test('Renders a link to redirect', () => {
    const book = {
        "name": "City of Bones (The Mortal Instruments, #1)1",
        "price": 14611,
        "authorName": "Cassandra Clare1",
        "imageUrl": "https://images.gr-assets.com/books/1432730315m/256683.jpg"
    };
    render(<BrowserRouter><Book book={book}/></BrowserRouter>);
    expect(screen.getByRole('link')).toBeInTheDocument();
});

