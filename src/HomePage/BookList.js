import {useEffect, useState} from "react";
import PaginatedBookList from "./PaginatedBookList";
import './BookList.css'
import './Loader.css'
import {useLocation} from "react-router";
import runtimeEnv from "@mars/heroku-js-runtime-env";
import Fade from "react-reveal/Fade";
import Loader from "../BookDetails/Loader";
import InfiniteScroll from "react-infinite-scroll-component";
import {SortButton} from "./SortButton";


function BookList(props) {
    function useQuery() {
        return new URLSearchParams(useLocation().search);
    }

    let query = useQuery();
    const initialPageNumber = parseInt(query.get("pageNumber") || 1);
    const [pageNumber, setPageNumber] = useState(initialPageNumber);
    const [totalPages, setTotalPages] = useState(1);
    const initialSortOrder = (query.get("order") || 'desc');
    const [sortOrder, setSortOrder] = useState(initialSortOrder);
    const searchKeyword = (query.get("search") || "");
    const [loading, updateLoading] = useState(true);
    const pageSize = props.pageSize || 9;
    const MAX_SIZE = 5000;
    const [books, updateBooks] = useState([]);

    const [hasMore, sethasMore] = useState(true);

    const [page, setpage] = useState(2);

    const sortOrderHandler = (offset) => {
        setpage(1);
        if (offset !== sortOrder) {
            setSortOrder(offset);
            updateLoading(true);
            sethasMore(true);
        }
    }

    function constructSearch(sortOrder) {
        const searchParam = query.get("search");
        const queryParams = []
        searchParam && queryParams.push(`search=${searchParam}`);
        queryParams.push(`order=${sortOrder}`);
        return `?${queryParams.join('&')}`;
    }

    useEffect(() => {
        const getLength = async () => {
            const env = runtimeEnv();
            const URL = env.REACT_APP_API_URL + `books?pageSize=${MAX_SIZE}&pageNumber=1&search=${searchKeyword}&sortBy=price&order=${sortOrder}`;
            const response = await fetch(URL);
            const parsedJson = await response.json();

            setTotalPages(Math.ceil(parsedJson.length / pageSize));
        }
        getLength();
    }, []);

    useEffect(() => {
        const getBooks = async () => {
            const env = runtimeEnv();
            const URL = env.REACT_APP_API_URL + `books?pageSize=${pageSize}&pageNumber=1&search=${searchKeyword}&sortBy=price&order=${sortOrder}`;
            const response = await fetch(URL);
            const parsedJson = await response.json();
            updateBooks(parsedJson);
            updateLoading(false);
        }
        getBooks();
    }, [sortOrder]);


    const fetchBooks = async () => {
        const env = runtimeEnv();
        const URL = env.REACT_APP_API_URL + `books?pageSize=${pageSize}&pageNumber=${page}&search=${searchKeyword}&sortBy=price&order=${sortOrder}`;
        const response = await fetch(URL);
        const parsedJson = await response.json();
        return parsedJson;
    };

    const fetchData = async () => {
        const moreBooks = await fetchBooks();

        updateBooks([...books, ...moreBooks]);
        if (moreBooks.length < 9) {
            sethasMore(false);
        }
        setpage(page + 1);
    };
    if (loading) {
        return (<Loader/>);
    } else if (books.length === 0) {
        return (
            <div className="noBooks">
                <h3 className="headno">We couldn't find any result for "<span className="skey">{searchKeyword}</span>"
                </h3>
                <ul>
                    <li className="inst">check your spelling</li>
                    <li className="inst">use more generic search terms</li>
                    <li className="inst">the product you are searching for may be discontinued</li>
                </ul>
                <a className="linkmarg" href="/">Go to Home</a>
            </div>
        )
    }
    return (
        <div style={{marginLeft: "200px"}}>
            <SortButton onClickAsc={() => sortOrderHandler('asc')} searchAsc={constructSearch('asc')}
                        onClickDesc={() => sortOrderHandler('desc')} searchDesc={constructSearch('desc')}/>
            <InfiniteScroll
                dataLength={books.length} //This is important field to render the next data
                next={fetchData}
                hasMore={books.length < 9 ? false : hasMore}
                loader={<Loader/>}
                endMessage={
                    <Fade bottom>
                        <p style={{textAlign: "center"}}>
                            <b>Yay! You have seen it all</b>
                        </p>
                    </Fade>
                }
            >
                <Fade bottom>
                    <PaginatedBookList books={books}/>
                </Fade>

            </InfiniteScroll>
        </div>
    );

}

export default BookList;