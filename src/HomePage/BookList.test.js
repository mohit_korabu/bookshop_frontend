import BookList from "./BookList";
import {fireEvent, render, screen, waitFor} from "@testing-library/react";
import {Router} from "react-router";
import {createMemoryHistory} from "history";
import {Route} from "react-router-dom";
import runtimeEnv from "@mars/heroku-js-runtime-env";

describe('BookList', () => {
    describe('search books', () => {

        beforeAll(() => jest.spyOn(window, 'fetch'));

        test('test for search books', async () => {
            const history = createMemoryHistory()
            history.push('/?search=city')
            let firstBook = {
                "id": 7,
                "name": "City of Bones (The Mortal Instruments, #1)1",
                "price": 14611,
                "authorName": "Cassandra Clare1",
                "imageUrl": "https://images.gr-assets.com/books/1432730315m/256683.jpg"
            };
            const response = [
                firstBook
            ];
            window.fetch
                .mockResolvedValueOnce({
                    ok: true,
                    json: async () => response,
                }).mockResolvedValueOnce({
                ok: true,
                json: async () => response,
            })

            render(
                <Router history={history}>
                    <Route path="/">
                        <BookList search={"city"}/>
                    </Route>
                </Router>
            );

            const env = runtimeEnv();

            let firstRequestURL = env.REACT_APP_API_URL + 'books?pageSize=5000&pageNumber=1&search=city&sortBy=price&order=desc';
            let secondRequestURL = env.REACT_APP_API_URL + 'books?pageSize=9&pageNumber=1&search=city&sortBy=price&order=desc';

            expect(window.fetch).toHaveBeenCalledWith(firstRequestURL);
            expect(window.fetch).toHaveBeenCalledWith(secondRequestURL);
            expect(window.fetch).toHaveBeenCalledTimes(2);

            await waitFor(() => screen.getByText(/city/i));
            expect(screen.getByText(/city of bones/i));
        });

        test('test for book not found', async () => {
            const history = createMemoryHistory()
            history.push('/?search=venkatesh')
            const response = [
            ];
            window.fetch
                .mockResolvedValueOnce({
                    ok: true,
                    json: async () => response,
                }).mockResolvedValueOnce({
                ok: true,
                json: async () => response,
            })

            render(
                <Router history={history}>
                    <Route path="/">
                        <BookList search={"venkatesh"}/>
                    </Route>
                </Router>
            );

            const env = runtimeEnv();

            let firstRequestURL = env.REACT_APP_API_URL + 'books?pageSize=5000&pageNumber=1&search=venkatesh&sortBy=price&order=desc';
            let secondRequestURL = env.REACT_APP_API_URL + 'books?pageSize=9&pageNumber=1&search=venkatesh&sortBy=price&order=desc';

            expect(window.fetch).toHaveBeenCalledWith(firstRequestURL);
            expect(window.fetch).toHaveBeenCalledWith(secondRequestURL);
            expect(window.fetch).toHaveBeenCalledTimes(2);

            await waitFor(() => screen.getByText(/We couldn't find any result/i));
            expect(screen.getByText(/venkatesh/i));
        });
    });

    describe('sort books', () => {

        beforeAll(() => jest.spyOn(window, 'fetch'));

        test('test for sort books in ascending order', async () => {
            const history = createMemoryHistory()
            history.push('/?search=city&order=asc')
            let firstBook = {
                "id": 7,
                "name": "City of Bones (The Mortal Instruments, #1)1",
                "price": 14611,
                "authorName": "Cassandra Clare1",
                "imageUrl": "https://images.gr-assets.com/books/1432730315m/256683.jpg"
            };
            let secondBook = {
                "id": 10,
                "name": "City of Coders",
                "price": 1000,
                "authorName": "Cassandra Clare1",
                "imageUrl": "https://images.gr-assets.com/books/1432730315m/256683.jpg"
            };
            const response = [
                secondBook,
                firstBook
            ];
            window.fetch
                .mockResolvedValueOnce({
                    ok: true,
                    json: async () => response,
                }).mockResolvedValueOnce({
                ok: true,
                json: async () => response,
            })

            render(
                <Router history={history}>
                    <Route path="/">
                        <BookList search={"city"} order={'asc'}/>
                    </Route>
                </Router>
            );

            const env = runtimeEnv();

            let firstRequestURL = env.REACT_APP_API_URL + 'books?pageSize=5000&pageNumber=1&search=city&sortBy=price&order=asc';
            let secondRequestURL = env.REACT_APP_API_URL + 'books?pageSize=9&pageNumber=1&search=city&sortBy=price&order=asc';

            expect(window.fetch).toHaveBeenCalledWith(firstRequestURL);
            expect(window.fetch).toHaveBeenCalledWith(secondRequestURL);
            expect(window.fetch).toHaveBeenCalledTimes(2);

            await waitFor(() => screen.getAllByText(/city/i));
            expect(screen.getAllByText(/city/i));
            const listOfBooks = screen.getAllByRole('heading', {level: 5});
            expect(listOfBooks[0].textContent).toContain("City of Coders");
            expect(listOfBooks[1].textContent).toContain("City of Bones");
        });

        test('test for sort books in descending order', async () => {
            const history = createMemoryHistory()
            history.push('/?search=city&order=desc')
            let firstBook = {
                "id": 7,
                "name": "City of Bones (The Mortal Instruments, #1)1",
                "price": 14611,
                "authorName": "Cassandra Clare1",
                "imageUrl": "https://images.gr-assets.com/books/1432730315m/256683.jpg"
            };
            let secondBook = {
                "id": 10,
                "name": "City of Coders",
                "price": 1000,
                "authorName": "Cassandra Clare1",
                "imageUrl": "https://images.gr-assets.com/books/1432730315m/256683.jpg"
            };
            const response = [
                firstBook,
                secondBook
            ];
            window.fetch
                .mockResolvedValueOnce({
                    ok: true,
                    json: async () => response,
                }).mockResolvedValueOnce({
                ok: true,
                json: async () => response,
            })

            render(
                <Router history={history}>
                    <Route path="/">
                        <BookList search={"city"} order={'desc'}/>
                    </Route>
                </Router>
            );

            const env = runtimeEnv();

            let firstRequestURL = env.REACT_APP_API_URL + 'books?pageSize=5000&pageNumber=1&search=city&sortBy=price&order=desc';
            let secondRequestURL = env.REACT_APP_API_URL + 'books?pageSize=9&pageNumber=1&search=city&sortBy=price&order=desc';

            expect(window.fetch).toHaveBeenCalledWith(firstRequestURL);
            expect(window.fetch).toHaveBeenCalledWith(secondRequestURL);
            expect(window.fetch).toHaveBeenCalledTimes(2);

            await waitFor(() => screen.getAllByText(/city/i));
            expect(screen.getAllByText(/city/i));
            const listOfBooks = screen.getAllByRole('heading', {level: 5});

            expect(listOfBooks[0].textContent).toContain("City of Bones");

            expect(listOfBooks[1].textContent).toContain("City of Coders");
        });

        test('sort books in ascending order after changing sort order to asc', async () => {
            const history = createMemoryHistory()
            history.push('/?order=desc')
            let firstBook = {
                "id": 7,
                "name": "City of Bones (The Mortal Instruments, #1)1",
                "price": 14611,
                "authorName": "Cassandra Clare1",
                "imageUrl": "https://images.gr-assets.com/books/1432730315m/256683.jpg"
            };
            let secondBook = {
                "id": 10,
                "name": "City of Coders",
                "price": 1000,
                "authorName": "Cassandra Clare1",
                "imageUrl": "https://images.gr-assets.com/books/1432730315m/256683.jpg"
            };

            const response = [
                firstBook,
                secondBook
            ];
            window.fetch
                .mockResolvedValueOnce({
                    ok: true,
                    json: async () => response,
                }).mockResolvedValueOnce({
                ok: true,
                json: async () => response,
            })

            render(
                <Router history={history}>
                    <Route path="/">
                        <BookList  order={'desc'}/>
                    </Route>
                </Router>
            );

            const env = runtimeEnv();

            let firstRequestURL = env.REACT_APP_API_URL + 'books?pageSize=5000&pageNumber=1&search=&sortBy=price&order=desc';
            let secondRequestURL = env.REACT_APP_API_URL + 'books?pageSize=9&pageNumber=1&search=&sortBy=price&order=desc';

            expect(window.fetch).toHaveBeenCalledWith(firstRequestURL);
            expect(window.fetch).toHaveBeenCalledWith(secondRequestURL);
            expect(window.fetch).toHaveBeenCalledTimes(2);

            await waitFor(() => screen.getAllByText(/city/i));
            expect(screen.getAllByText(/city/i));
            const listOfBooks = screen.getAllByRole('heading', {level: 5});
            expect(listOfBooks[1].textContent).toContain("City of Coders");
            expect(listOfBooks[0].textContent).toContain("City of Bones");

            fireEvent.click(screen.getByText(/▲/));

        });
    });
});
