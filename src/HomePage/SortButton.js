import {Component} from "react";
import {Link} from "react-router-dom";
import * as PropTypes from "prop-types";

export class SortButton extends Component {
    render() {
        return <div style={{margin: "135px"}}>
            <nav className={"sorting"}
                 style={{
                     position: "absolute",
                     right: 540,
                     top: 160,
                 }}>
                <h3>Sort By Price</h3>
                <button className="aB" onClick={this.props.onClickAsc}
                        style={{
                            position: "absolute",
                            right: -40,
                            top: 22,
                        }}>
                    <Link to={{pathname: "/", search: this.props.searchAsc}}>
                        ▲
                    </Link>
                </button>

                <button className="aB" onClick={this.props.onClickDesc}
                        style={{
                            position: "absolute",
                            right: -70,
                            top: 22,
                        }}>
                    <Link to={{pathname: "/", search: this.props.searchDesc}}>
                        ▼
                    </Link>
                </button>
            </nav>
        </div>;
    }
}

SortButton.propTypes = {
    onClickAsc: PropTypes.func,
    searchAsc: PropTypes.any,
    onClickDesc: PropTypes.func,
    searchDesc: PropTypes.any
};