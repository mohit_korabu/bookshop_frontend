import {render, screen} from "@testing-library/react";
import PaginatedBookList from "./PaginatedBookList";
import {BrowserRouter} from "react-router-dom";

beforeAll(() => jest.spyOn(window, 'fetch'))

test('renders one book', async () => {
    let firstBook = {id:1, name:'name', authorName:'authorName', price: 190, imageUrl:''};
    const books = [firstBook];

    render(<BrowserRouter><PaginatedBookList books={books}/></BrowserRouter>);

    expect(screen.getByText('By authorName')).toBeInTheDocument();
    expect(screen.getByText('₹190')).toBeInTheDocument();
});

test('renders two books in one page', async () => {
    let firstBook = {id:1, name:'Breaking Dawn', authorName:'Stephenie', price: 100, imageUrl:''};
    let secondBook = {id:2, name:'name', authorName:'authorName', price: 190, imageUrl:''};
    const response = [firstBook, secondBook];


    render(<BrowserRouter><PaginatedBookList books={response}/></BrowserRouter>);

    expect(screen.getByText('By Stephenie')).toBeInTheDocument();
    expect(screen.getByText('name')).toBeInTheDocument();
    expect(screen.getByText('By authorName')).toBeInTheDocument();
    expect(screen.getByText('₹100')).toBeInTheDocument();
});