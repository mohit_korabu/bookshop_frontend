import {render} from '@testing-library/react';
import App from './App';
import {Route, Router} from "react-router-dom";
import {createMemoryHistory} from "history";

test('renders learn react link', () => {
    const history = createMemoryHistory();
    history.push('/');
    render(
        <Router history={history}>
            <Route path="/">
                <App/>
            </Route>
        </Router>
    );
});