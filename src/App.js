import './App.css';
import BookDetails from "./BookDetails/BookDetails";
import {Route, BrowserRouter as Router, Switch} from "react-router-dom";
import BookList from "./HomePage/BookList";
import Order from "./Order/Order";
import OrderSummary from "./Order/OrderSummary";
import OrderStatus from "./Order/OrderStatus";
import BaseTemplate from "./BaseTemplate/BaseTemplate";
import React from "react";

function App() {
    return (
        <Router>
            <div className="container-fluid tempmargin">
                <BaseTemplate/>
            </div>
            <div>
                <Switch>
                    <Route exact path="/order/new">
                        <Order/>
                    </Route>
                    <Route exact path="/order/:orderId">
                        <OrderSummary/>
                    </Route>
                    <Route exact path="/order/:orderId/status">
                        <OrderStatus/>
                    </Route>
                    <Route path="/books/:bookId">
                        <BookDetails/>
                    </Route>
                    <Route path="/">
                        <BookList/>
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}

export default App;
